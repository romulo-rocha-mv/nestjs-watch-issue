import { Controller, Get } from '@nestjs/common';

import { Asset } from '../../entities/assets.entity';
import { AssetsService } from '../../services/assets/assets.service';

@Controller('assets')
export class AssetsController {
  constructor(private assetsService: AssetsService) {}

  @Get()
  async findAll(): Promise<Asset[]> {
    return this.assetsService.findAll();
  }
}
