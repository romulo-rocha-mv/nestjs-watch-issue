import {
  Column,
  CreatedAt,
  Table,
  UpdatedAt,
  Model,
} from 'sequelize-typescript';

@Table({ tableName: 'assets' })
export class Asset extends Model<Asset> {
  @Column
  userId: number;

  @Column('tinytext')
  url: string;

  @Column('varchar')
  uniqueHash: string;

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;
}
