import { Sequelize } from 'sequelize-typescript';

import { Asset } from '../entities/assets.entity';

export const databaseProviders = [
  {
    provide: 'SEQUELIZE',
    useFactory: async () => {
      const sequelize = new Sequelize({
        dialect: 'mysql',
        host: process.env.DATABASE_HOST,
        port: parseInt(process.env.DATABASE_PORT, 10),
        username: process.env.DATABASE_USERNAME,
        password: process.env.DATABASE_PASSWORD,
        database: process.env.DATABASE_NAME,
      });

      sequelize.addModels([Asset]);
      await sequelize.sync();

      return sequelize;
    },
  },
];
