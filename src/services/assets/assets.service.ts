import { Inject, Injectable } from '@nestjs/common';
import { Asset } from '../../entities/assets.entity';

@Injectable()
export class AssetsService {
  constructor(
    @Inject('ASSETS_REPOSITORY') private assetsRepository: typeof Asset,
  ) {}

  async findAll(): Promise<Asset[]> {
    return this.assetsRepository.findAll<Asset>();
  }
}
