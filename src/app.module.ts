import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { assetsProviders } from './providers/assets.providers';
import { AssetsService } from './services/assets/assets.service';
import { AssetsController } from './controllers/assets/assets.controller';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: ['.env', '.env.dev', '.env.local'],
    }),
  ],
  controllers: [AppController, AssetsController],
  providers: [AppService, AssetsService, ...assetsProviders],
})
export class AppModule {}
