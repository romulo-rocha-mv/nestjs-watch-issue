import { Asset } from '../entities/assets.entity';

export const assetsProviders = [
  {
    provide: 'ASSETS_REPOSITORY',
    useValue: Asset,
  },
];
